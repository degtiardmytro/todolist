package com.home_task_5;

import com.home_task_5.models.TODOTask;
import com.home_task_5.utils.PathUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
@WebServlet("/")
public class TaskController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Method method = PathUtils.getMethod(request.getRequestURI(), TaskController.class);
        if(method != null){
            PathUtils.callMethod(method, this, request, response);
        }else{
            this.actionView(request, response);
        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Method method = PathUtils.getMethod(request.getRequestURI(), TaskController.class);
        if(method != null){
            PathUtils.callMethod(method, this, request, response);
        }else{
            this.actionView(request, response);
        }
    }




    /**
     *
     * @param request -
     * @param response -
     */
    private void actionView(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/view.jsp");
        dispatcher.forward(request, response);
    }


    /**
     *
     * @param request -
     * @param response -
     * @throws ServletException -
     * @throws IOException -
     */
    private void actionCreate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        String method = request.getMethod();
        if(method.equals("GET")){
            RequestDispatcher createDispatcher = getServletContext().getRequestDispatcher("/create.jsp");
            createDispatcher.forward(request, response);
        }else if(method.equals("POST")){

            HttpSession session = request.getSession();
            String taskName = request.getParameter("taskName");
            String taskDescription = request.getParameter("taskDescription");
            Byte taskStatus = Byte.parseByte(request.getParameter("taskStatus"));

            Object tasks = session.getAttribute("taskList");
            Map<Long, TODOTask> taskList = (tasks != null && tasks instanceof Map) ? (Map<Long, TODOTask>) tasks : new HashMap<>();


            if(taskName != null && !taskName.isEmpty()){
                TODOTask newTask = new TODOTask(taskName, taskDescription);
                Long taskId = System.currentTimeMillis();
                newTask.setId(taskId);
                newTask.setStatus(taskStatus);
                taskList.put(taskId, newTask);
                session.setAttribute("taskList", taskList);
            }
            response.sendRedirect("/");
        }

    }


    /**
     *
     * @param request -
     * @param response -
     * @throws ServletException -
     * @throws IOException -
     */
    private void actionUpdate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{

        String method = request.getMethod();
        if(method.equals("GET")){

            HttpSession session = request.getSession();
            Object tasksObj = session.getAttribute("taskList");
            Object idObj = request.getParameter("id");

            if(tasksObj != null && tasksObj instanceof Map && idObj != null){
                Map<Long, TODOTask> taskList = (Map<Long, TODOTask>) tasksObj;
                Long id = Long.parseLong(idObj.toString());

                TODOTask updatedTask = taskList.get(id);
                if(updatedTask != null){
                    request.setAttribute("taskName", updatedTask.getName());
                    request.setAttribute("taskDescription", updatedTask.getDesription());
                    request.setAttribute("taskStatus", updatedTask.getStatus());
                    request.setAttribute("id", id);
                    RequestDispatcher updateDispatcher = getServletContext().getRequestDispatcher("/update.jsp");
                    updateDispatcher.forward(request, response);
                }
            }
        }else if(method.equals("POST")){
            HttpSession session = request.getSession();
            Object tasksObj = session.getAttribute("taskList");
            Object idObj = request.getParameter("id");
            if(tasksObj != null && tasksObj instanceof Map && idObj != null){
                Map<Long, TODOTask> taskList = (Map<Long, TODOTask>) tasksObj;
                Long id = Long.parseLong(idObj.toString());
                TODOTask updatedTask = taskList.get(id);
                if(updatedTask != null){
                    Object taskNameObj = request.getParameter("taskName");
                    if(taskNameObj != null){
                        updatedTask.setName(taskNameObj.toString());
                    }
                    Object taskDescriptionObj = request.getParameter("taskDescription");
                    if(taskDescriptionObj != null){
                        updatedTask.setDesription(taskDescriptionObj.toString());
                    }
                    String taskStatusObj = request.getParameter("taskStatus");
                    if(taskStatusObj != null){
                        updatedTask.setStatus(Byte.parseByte(taskStatusObj));
                    }
                    session.setAttribute("taskList", taskList);
                }
            }
            response.sendRedirect("/");
        }
    }

    /**
     *
     * @param request -
     * @param response -
     * @throws ServletException -
     * @throws IOException -
     */
    private void actionDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{

        HttpSession session = request.getSession();
        Object tasksObj = session.getAttribute("taskList");
        Object idObj = request.getParameter("id");
        if(tasksObj != null && tasksObj instanceof Map && idObj != null){
            Map<Long, TODOTask> taskList = (Map<Long, TODOTask>) tasksObj;
            Long id = Long.parseLong(idObj.toString());
            TODOTask removeTask = taskList.get(id);
            if(removeTask != null && taskList.remove(id, removeTask)){
                session.setAttribute("taskList", taskList);
            }
        }
        response.sendRedirect("/");

    }



}
