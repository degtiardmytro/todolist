package com.home_task_5.models;

/**
 *
 */
public class TODOTask {

    public static final byte TASK_STATUS_NOT_DONE = 0;
    public static final byte TASK_STATUS_DONE = 1;
    public static final byte TASK_STATUS_REMAKE = 2;


    private long id;
    private String name;
    private String desription;
    private byte status;

    public TODOTask(String name, String desription) {
        this.name = name;
        this.desription = desription;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesription() {
        return desription;
    }

    public void setDesription(String desription) {
        this.desription = desription;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

}
