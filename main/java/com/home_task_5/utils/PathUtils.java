package com.home_task_5.utils;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 *
 */
public class PathUtils {

    public static Method getMethod(String requestUri, Class<?> clazz){

        String action = null;
        String[] params = requestUri.split("/");
        if(params.length >= 2 && !params[1].isEmpty()){
            action = String.format("action%s%s", params[1].substring(0, 1).toUpperCase(), params[1].substring(1));
        }
        if(action != null){
            for (Method method: clazz.getDeclaredMethods()) {
                String methodName = method.getName();
                if(action.equals(methodName)){
                    return method;
                }
            }
        }
        return null;
    }



    public static void callMethod(Method method, Object target, Object...args){

        if(method != null && target != null){
            try{
                method.setAccessible(true);
                method.invoke(target, args);
            }catch (IllegalAccessException e1){
                //Logger.log(e1.getMessage());
            }catch (InvocationTargetException e2){
                //Logger.log(e2.getMessage());
                e2.printStackTrace();
            }
        }
    }


}
