<%@ page import="com.home_task_5.models.TODOTask" %>
<%
    Object actionObj = request.getAttribute("formAction");
    String action = actionObj != null ? actionObj.toString() : "";
    Object submitNameObj = request.getAttribute("submitName");
    String submitName = submitNameObj != null ? submitNameObj.toString() : "";

    Object idObj = request.getAttribute("id");
    String id = idObj != null ? idObj.toString() : "";
    Object taskNameObj = request.getAttribute("taskName");
    String taskName = taskNameObj != null ? taskNameObj.toString() : "";
    Object taskDescriptionObj = request.getAttribute("taskDescription");
    String taskDescription = taskDescriptionObj != null ? taskDescriptionObj.toString() : "";
    Object taskStatusObj = request.getAttribute("taskStatus");
    Byte taskStatus = taskStatusObj != null ? (Byte) taskStatusObj : 0;

%>
<form action="<%= action%>" method="post">
    <label>
        <span>Task name</span>
        <input type="text" name="taskName" value="<%= taskName%>">
    </label><br>
    <label>
        <span>Task description</span>
        <textarea name="taskDescription"><%= taskDescription%></textarea>
    </label><br>
    <label>
        <span>Task status</span>
        <select name="taskStatus">
            <option value="0">not done</option>
            <option value="1" <%=taskStatus.equals(TODOTask.TASK_STATUS_DONE)?"selected":""%>>done</option>
        </select>
    </label><br>
    <input type="hidden" name="id" value="<%=id%>">
    <input type="submit" value="<%= submitName%>" style="cursor: pointer;">
</form>