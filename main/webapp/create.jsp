<%--
  Created by IntelliJ IDEA.
  User: dimas
  Date: 18.07.17
  Time: 21:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String pageTitle = "CREATE TASK";
    request.setAttribute("submitName", pageTitle);
    request.setAttribute("formAction", "/create");
%>
<html>
<head>
    <title><%= pageTitle%></title>
</head>
<body>

    <h1><%= pageTitle%></h1>
    <%@include file="_form.jsp"%>

</body>
</html>
