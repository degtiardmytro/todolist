<%@ page import="java.util.List" %>
<%@ page import="com.home_task_5.models.TODOTask" %>
<%@ page import="java.util.Map" %><%--
  Created by IntelliJ IDEA.
  User: dimas
  Date: 18.07.17
  Time: 17:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h1>TASK LIST</h1>
        <%
            Object tasks = session.getAttribute("taskList");
            if(tasks != null && tasks instanceof Map){
                Map<Long, TODOTask> taskList = (Map<Long, TODOTask>)tasks;
                if(taskList.size() > 0){
        %>
        <table cellspacing="2" border="1" cellpadding="5" width="600">
            <thead>
                <tr>
                    <td>#</td>
                    <td>Task name</td>
                    <td>Task description</td>
                    <td>Status</td>
                    <td>Options</td>
                </tr>
            </thead>
            <tbody>
        <%
            int count = 0;
            for(Map.Entry<Long, TODOTask> entry: taskList.entrySet()){
                TODOTask task = entry.getValue();
                Boolean statusDone = task.getStatus()==TODOTask.TASK_STATUS_DONE;
        %>
                    <tr>
                        <td><%=++count%></td>
                        <td><%= task.getName()%></td>
                        <td><%= task.getDesription()%></td>
                        <td><%= statusDone ? "done" : "not done"%></td>
                        <td>
                            <a href="/update?id=<%= task.getId()%>"><button style="cursor: pointer;">Update</button></a>
                            <%if(statusDone){%>
                                <a href="/delete?id=<%= task.getId()%>"><button style="cursor: pointer;">Delete</button></a>
                            <%}%>
                        </td>
                    </tr>
        <%}%>
            </tbody>
        </table>
        <%}else{%>
                        <h4>No tasks in your task-list</h4>
    <%}}else{%>
        <h4>No tasks in your task-list</h4>
    <%}%>
    <a href="/create"><button style="cursor: pointer;">Create new task</button></a>
</body>
</html>
